---
date: 'Tue 09 Jun 2020 01:53:39 PM PKT'
layout: page
aliases: ['about-us', 'aboutme', 'about', 'contact', 'social']
author: 
  name: "Abdullah"
---

{{< avatar "avatar.webp" "Abdullah" >}}


        Information Security Analyst | Pythonista | YouTuber


  {{< social_links gitlab="Abdullah" twitter="AbdullahToday" youtube="AbdullahToday" facebook="AbdullahToday" telegram="AbdullahToday" web="abdullah.today" >}}
