---
title: "No one should abuse their parents"
date: 2020-08-16T15:37:16+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic

حَدَّثَنَا أَحْمَدُ بْنُ يُونُسَ ، حَدَّثَنَا إِبْرَاهِيمُ بْنُ سَعْدٍ ، عَنْ أَبِيهِ ، عَنْ حُمَيْدِ بْنِ عَبْدِ الرَّحْمَنِ ، عَنْ عَبْدِ اللَّهِ بْنِ عَمْرٍو رَضِيَ اللَّهُ عَنْهُمَا ، قَالَ : قَالَ رَسُولُ اللَّهِ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ :    إِنَّ مِنْ أَكْبَرِ الْكَبَائِرِ أَنْ يَلْعَنَ الرَّجُلُ وَالِدَيْهِ    قِيلَ ، يَا رَسُولَ اللَّهِ ، وَكَيْفَ يَلْعَنُ الرَّجُلُ وَالِدَيْهِ ؟ قَالَ :    يَسُبُّ الرَّجُلُ أَبَا الرَّجُلِ فَيَسُبُّ أَبَاهُ وَيَسُبُّ أُمَّهُ

### English

Narrated `Abdullah bin `Amr: Allah's Apostle said.   It is one of the greatest sins that a man should curse his parents.   It was asked (by the people),   O Allah's Apostle! How does a man curse his parents?   The Prophet said,   'The man abuses the father of another man and the latter abuses the father of the former and abuses his mother.

### Urdu

رسول اللّٰهﷺ نے فرمایا: یقیناً سب سے بڑے گناہوں میں سے یہ ہے کہ کوئی شخص اپنے والدین پر لعنت بھیجے۔ پوچھا گیا: یا رسول اللّٰه! کوئی شخص اپنے ہی والدین پر کیسے لعنت بھیجے گا؟ نبی ﷺ نے فرمایا کہ وہ شخص دوسرے کے باپ کو برا بھلا کہے گا تو دوسرا بھی اس کے باپ کو اور اس کی ماں کو برا بھلا کہے گا۔

### Book

Sahi Bukhari


### Status

Sahi

### Reference

Sahi Bukhari Hadees # 5973
