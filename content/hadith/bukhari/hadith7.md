---
title: "Facilitate things to people"
date: 2020-08-20T16:03:15+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic

حَدَّثَنَا مُحَمَّدُ بْنُ بَشَّارٍ ، قَالَ : حَدَّثَنَا يَحْيَى بْنُ سَعِيدٍ ، قَالَ : حَدَّثَنَا شُعْبَةُ ، قَالَ : حَدَّثَنِي أَبُو التَّيَّاحِ ، عَنْ أَنَسِ ، عَنِ النَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، قَالَ :    يَسِّرُوا وَلَا تُعَسِّرُوا ، وَبَشِّرُوا وَلَا تُنَفِّرُوا    .

### English

Narrated Anas bin Malik: The Prophet said,   Facilitate things to people (concerning religious matters), and do not make it hard for them and give them good tidings and do not make them run away (from Islam).

### Urdu

آپ ﷺ نے فرمایا، آسانی کرو اور سختی نہ کرو اور خوش کرو اور نفرت نہ دلاؤ۔

### Book

Sahi Bukhari



### Status

Sahi

### Reference

Sahi Bukhari Hadees # 69
