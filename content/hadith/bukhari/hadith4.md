---
title: "Following the Sunnah of Prophet PBUH"
date: 2020-08-19T21:10:02+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic

حَدَّثَنَا مُحَمَّدُ بْنُ سِنَانٍ ، حَدَّثَنَا فُلَيْحٌ ، حَدَّثَنَا هِلَالُ بْنُ عَلِيٍّ ، عَنْ عَطَاءِ بْنِ يَسَارٍ ، عَنْ أَبِي هُرَيْرَةَ ، أَنَّ رَسُولَ اللَّهِ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، قَالَ :    كُلُّ أُمَّتِي يَدْخُلُونَ الْجَنَّةَ إِلَّا مَنْ أَبَى ، قَالُوا : يَا رَسُولَ اللَّهِ ، وَمَنْ يَأْبَى ؟ ، قَالَ : مَنْ أَطَاعَنِي دَخَلَ الْجَنَّةَ ، وَمَنْ عَصَانِي فَقَدْ أَبَى    .

### English

Narrated Abu Huraira: Allah's Apostle said,   All my followers will enter Paradise except those who refuse.   They said,   O Allah's Apostle! Who will refuse?   He said,   Whoever obeys me will enter Paradise, and whoever disobeys me is the one who refuses (to enter it).

### Urdu

 رسول اللّٰه ﷺ نے فرمایا ”ساری امت جنت میں جائے گی سوائے ان کے جنہوں نے انکار کیا۔“ صحابہ نے عرض کیا: یا رسول اللّٰه! انکار کون کرے گا؟ فرمایا ”جو میری اطاعت کرے گا وہ جنت میں داخل ہو گا اور جو میری نافرمانی کرے گا اس نے انکار کیا۔“

### Book

Sahi Bukhari


### Status

Sahi

### Reference

Sahi Bukhari Hadees # 7280
