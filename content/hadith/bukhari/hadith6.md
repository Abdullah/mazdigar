---
title: "A small group should greet a large group"
date: 2020-08-19T21:15:00+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic

حَدَّثَنَا مُحَمَّدُ بْنُ مُقَاتِلٍ أَبُو الْحَسَنِ ، أَخْبَرَنَا عَبْدُ اللَّهِ ، أَخْبَرَنَا مَعْمَرٌ ، عَنْ هَمَّامِ بْنِ مُنَبِّهٍ ، عَنْ أَبِي هُرَيْرَةَ ، عَنِ النَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، قَالَ :    يُسَلِّمُ الصَّغِيرُ عَلَى الْكَبِيرِ ، وَالْمَارُّ عَلَى الْقَاعِدِ ، وَالْقَلِيلُ عَلَى الْكَثِيرِ    .


### English

Narrated Abu Huraira: The Prophet said,   The young should greet the old, the passer by should greet the sitting one, and the small group of persons should greet the large group of persons.

### Urdu

 نبی کریم ﷺ نے فرمایا ”چھوٹا بڑے کو سلام کرے، گزرنے والا بیٹھنے والے کو سلام کرے اور چھوٹی جماعت بڑی جماعت کو پہلے سلام کرے۔“

### Book


Sahi Bukhari


### Status

Sahi

### Reference

Sahi Bukhari Hadees # 6231
