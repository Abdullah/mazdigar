---
title: "Sunset and Sunrise"
date: 2020-06-11T12:04:16+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
url: "/hod"
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: ["Hadith of the Day"]
---

### Arabic
حَدَّثَنَا مُحَمَّدٌ ، أَخْبَرَنَا عَبْدَةُ ، عَنْ هِشَامِ بْنِ عُرْوَةَ ، عَنْ أَبِيهِ ، عَنْ ابْنِ عُمَرَ رَضِيَ اللَّهُ عَنْهُمَا ، قَالَ : قَالَ رَسُولُ اللَّهِ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ :    إِذَا طَلَعَ حَاجِبُ الشَّمْسِ فَدَعُوا الصَّلَاةَ حَتَّى تَبْرُزَ وَإِذَا غَابَ حَاجِبُ الشَّمْسِ فَدَعُوا الصَّلَاةَ حَتَّى تَغِيبَ ،

### English

Narrated Ibn Umar:
	Allah's Apostle said, 
												When the (upper) edge of the sun appears (in the morning), don't perform a prayer till the sun appears in full, and when the lower edge of the sun sets.


### Urdu

 رسول اللہ صلی اللہ علیہ وسلم نے فرمایا ”جب سورج کا اوپر کا کنارہ نکل آئے تو نماز نہ پڑھو جب تک وہ پوری طرح ظاہر نہ ہو جائے اور جب غروب ہونے لگے تب بھی اس وقت تک کے لیے نماز چھوڑ دو جب تک بالکل غروب نہ ہو جائے۔“

### Book

Bukhari



### Status

Sahi

### Reference
Sahi Bukhari Hadees # 3272
