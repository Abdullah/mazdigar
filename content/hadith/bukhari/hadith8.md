---
title: "Allow women to come Masjid"
date: 2020-08-25T10:09:20+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic

حَدَّثَنَا عَبْدُ اللَّهِ بْنُ مُحَمَّدٍ ، حَدَّثَنَا شَبَابَةُ ، حَدَّثَنَا وَرْقَاءُ ، عَنْ عَمْرِو بْنِ دِينَارٍ ، عَنْ مُجَاهِدٍ ، عَنْ ابْنِ عُمَرَ ، عَنِ النَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، قَالَ :    ائْذَنُوا لِلنِّسَاءِ بِاللَّيْلِ إِلَى الْمَسَاجِدِ    .


### English


Narrated Ibn 'Umar: The Prophet (p.b.u.h) said,   Allow women to go to the Mosques at night.


### Urdu

نبی کریم صلی اللہ علیہ وسلم نے فرمایا عورتوں کو رات کے وقت مسجدوں میں آنے کی اجازت دے دیا کرو۔


### Book


Sahi Bukhari



### Status

Sahi

### Reference

Sahi Bukhari Hadees # 899
