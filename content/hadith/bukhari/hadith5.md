---
title: "The virtue of spending in the way of Allah"
date: 2020-08-19T21:12:40+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic

 حَدَّثَنَا عَلِيُّ بْنُ عَبْدِ اللَّهِ ، حَدَّثَنَا سُفْيَانُ ، قَالَ الزُّهْرِيُّ ، عَنْ سَالِمٍ ، عَنْ أَبِيهِ ، عَنِ النَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ ، قَالَ :    لَا حَسَدَ إِلَّا فِي اثْنَتَيْنِ : رَجُلٌ آتَاهُ اللَّهُ الْقُرْآنَ فَهُوَ يَتْلُوهُ آنَاءَ اللَّيْلِ وَآنَاءَ النَّهَارِ ، وَرَجُلٌ آتَاهُ اللَّهُ مَالًا فَهُوَ يُنْفِقُهُ آنَاءَ اللَّيْلِ وَآنَاءَ النَّهَارِ    ، سَمِعْتُ سُفْيَانَ مِرَارًا لَمْ أَسْمَعْهُ يَذْكُرُ الْخَبَرَ ، وَهُوَ مِنْ صَحِيحِ حَدِيثِهِ .

### English

Narrated Salim's father: The Prophet said,   Not to wish to be the like of except the like of two (persons): a man whom Allah has given the knowledge of the Qur'an and he recites it during the hours of the night and the hours of the day; and a man whom Allah has given wealth and he spends it (in Allah's Cause) during the hours of the night and during the hours of the day.

### Urdu

نبی کریم صلی اللہ علیہ وسلم نے فرمایا ”رشک کے قابل تو دو ہی آدمی ہیں۔ ایک وہ جسے اللہ نے قرآن دیا اور وہ اس کی تلاوت رات دن کرتا رہتا ہے اور دوسرا وہ جسے اللہ نے مال دیا ہو اور وہ اسے رات و دن خرچ کرتا رہا۔“ علی بن عبداللہ نے کہا کہ میں نے یہ حدیث سفیان بن عیینہ سے کئی بار سنی «أخبرنا» کے لفظوں کے ساتھ انہیں کہتا سنا باوجود اس کے ان کی یہ حدیث صحیح اور متصل ہے۔ 


### Book


Sahi Bukhari


### Status

Sahi

### Reference

Sahi Bukhari Hadees # 7529
