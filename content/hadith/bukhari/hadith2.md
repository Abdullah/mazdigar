---
title: "Slave of Dinar"
date: 2020-06-11T20:46:02+05:00
draft: false
cover: ""#
author: "Abdullah"
toc: true
comments: true
layout: post
tags: ["hadith", "hadith_of_day",  "sayings of Muhammad(PBUH)", "Hadees", "Bukhari", "Muslim"]
description: [""]
---

### Arabic
حَدَّثَنِي يَحْيَى بْنُ يُوسُفَ ، أَخْبَرَنَا أَبُو بَكْرٍ ، عَنْ أَبِي حَصِينٍ ، عَنْ أَبِي صَالِحٍ ، عَنْ أَبِي هُرَيْرَةَ رَضِيَ اللَّهُ عَنْهُ ، قَالَ : قَالَ رَسُولُ اللَّهِ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ :    تَعِسَ عَبْدُ الدِّينَارِ ، وَالدِّرْهَمِ ، وَالْقَطِيفَةِ ، وَالْخَمِيصَةِ ، إِنْ أُعْطِيَ رَضِيَ ، وَإِنْ لَمْ يُعْطَ لَمْ يَرْضَ    .

### English

Narrated Abu Huraira: 
	The Prophet said, Perish the slave of Dinar, Dirham, Qatifa (thick soft cloth), and Khamisa (a garment), for if he is given, he is pleased; otherwise he is dissatisfied.

### Urdu

 رسول اللہ صلی اللہ علیہ وسلم نے فرمایا ”دینار و درہم کے بندے، عمدہ ریشمی چادروں کے بندے، سیاہ کملی کے بندے، تباہ ہو گئے کہ اگر انہیں دیا جائے تو وہ خوش ہو جاتے ہیں اور اگر نہ دیا جائے تو ناراض رہتے ہیں۔“ 


### Book

Sahi Bukhari


### Status

Sahi 

### Reference

Sahi Bukhari # 6435
